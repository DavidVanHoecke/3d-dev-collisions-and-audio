using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPackage : MonoBehaviour
{
    [SerializeField]
    GameObject packagePrefab;

    [SerializeField]
    Vector3 position;

    // Start is called before the first frame update
    void Start()
    {
        if (packagePrefab == null)
            throw new InvalidOperationException("You must assign a package prefab");

        InvokeRepeating("SpawnPackage", 1f, 1f);
    }

    // Update is called once per frame
    void SpawnPackage()
    {
        Instantiate(packagePrefab, position, Quaternion.identity);
    }
}
