using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnTriggerEnter : MonoBehaviour
{
    [SerializeField]
    GameObject ExplosionPrefabThingy;



    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);

        var explosion = Instantiate(ExplosionPrefabThingy, gameObject.transform.position, Quaternion.identity);
        gameObject.SetActive(false);
        Destroy(this.gameObject, 3f);
        Destroy(explosion, 5f);
    }
}
