using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandlerImpactAudio : MonoBehaviour
{

    [SerializeField]
    private AudioClip[] impactAudioClips;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        if (audioSource != null)
        {
            audioSource.clip = impactAudioClips[Random.Range(0, impactAudioClips.Length)];
            audioSource.Play();
        }
    }
}
